#include "Vector3D.h"
#include "Wire.h"
#include <AutoPID.h>

#define trigPin A3      
#define echoPin A4      
#define avertisseur 13 
#define selectA A1
#define selectB A2

#define SENSOR_RX 11
#define SENSOR_TX 10
#define SENSOR_EN 9

#define CH0_START_L   0x06
#define CH0_START_H   0x07
#define CH0_END_L     0x08
#define CH0_END_H     0x09
#define CH1_START_L   0x0A
#define CH1_START_H   0x0B
#define CH1_END_L     0x0C
#define CH1_END_H     0x0D
#define CH2_START_L   0x0E
#define CH2_START_H   0x0F
#define CH2_END_L     0x10
#define CH2_END_H     0x11

#define NB_SAMPLES 20

//this is the tolerance admitted for the extremums removal. Admitted values : [value-TOLERANCE_PERCENTAGE*value/100, value+TOLERANCE_PERCENTAGE*value/100]
#define TOLERANCE_PERCENTAGE 5
#define MAX_ANGLE (PI/3.0)
#define HYSTERESIS_ANGLE_RAD (0.0*PI/180.0)

long distance[4];
long samples[4][NB_SAMPLES];
int nbAbberations[4];
long durations[4];
double distanceD[4];
double xlastAngle = 0;
double ylastAngle = 0;
double xSetPoint = 0;
double ySetPoint = 0;
double xAngle = 0;
double yAngle = 0;

//PID
AutoPID xPID(&xAngle, &xSetPoint, &xlastAngle, -MAX_ANGLE, MAX_ANGLE,1.0, 0, 1.0);
AutoPID yPID(&yAngle, &ySetPoint, &ylastAngle, -MAX_ANGLE, MAX_ANGLE,1.0, 0, 1.0);

uint8_t sensors[4] = { 2,0,1,3 }; //Clockwise row (from up to right to down to left to up)

uint16_t getServoValue(double angle)
{
	static uint16_t lastValue;
	double maxAngle = PI / 2.0;
	double minAngle = -PI / 2.0;
	double frequency = 56.0;
	double resolution = 4096.0;
	double zeroAnglePulseWidth = 1500;
	double travelPulseWidth = 600;
	double stepWidth = 1000000.0 / frequency / resolution ;
	
	if (angle < minAngle)
	{
		angle = minAngle;
	}
	else if (angle > maxAngle)
	{
		angle = maxAngle;
	}
	lastValue = (zeroAnglePulseWidth + angle / maxAngle * travelPulseWidth) / stepWidth;
	return lastValue;

}

void setup() {
	Serial.begin(9600);
	pinMode(trigPin, OUTPUT); 
	pinMode(selectA, OUTPUT);
	pinMode(selectB, OUTPUT);
	pinMode(echoPin, INPUT);   
	pinMode(avertisseur, OUTPUT);

	Wire.begin(); // join i2c bus (address optional for master)
				  //Set prescaler to achieve 50Hz MUST BE DONE BEFORE SLEEP DISABLE
	Wire.beginTransmission(0x40);
	Wire.write(0xFE); //select prescale register
	Wire.write(0x79);
	Wire.endTransmission();
	//address of servo hat is 0x40
	Wire.beginTransmission(0x40);
	Wire.write(0x00);
	Wire.write(0x00);
	Wire.endTransmission();

	//Channels 2 gives middle value
	Wire.beginTransmission(0x40);
	Wire.write(CH2_END_L);
	Wire.write((uint8_t)(((uint16_t)344) & 0x00FF));
	Wire.endTransmission();
	Wire.beginTransmission(0x40);
	Wire.write(CH2_END_H);
	Wire.write((uint8_t)(((uint16_t)344) >> 8));
	Wire.endTransmission();

	xPID.setTimeStep(100);
	yPID.setTimeStep(100);

}
// qsort requires you to create a sort function
void sort(long a[], int size) {
	for (int i = 0; i < (size - 1); i++) {
		for (int o = 0; o < (size - (i + 1)); o++) {
			if (a[o] > a[o + 1]) {
				long t = a[o];
				a[o] = a[o + 1];
				a[o + 1] = t;
			}
		}
	}
}

void loop() {
	
	uint32_t beginMillis = millis();
	
	//get NB_SAMPLES samples
	for (int j = 0; j < NB_SAMPLES; j++)
	{
		for (int i = 0; i < 4; i++)
		{
			//select channel
			digitalWrite(selectA, sensors[i]>>1);
			digitalWrite(selectB, sensors[i]&0x1);

			//make measurement
			digitalWrite(trigPin, LOW);
			delayMicroseconds(2);
			digitalWrite(trigPin, HIGH);
			delayMicroseconds(10); //Trig envois pendant 10ms 
			digitalWrite(trigPin, LOW);

			//wait for response
			samples[i][j] = pulseIn(echoPin, HIGH);
			//this helps
			delay(1);
		}
	}
	
	//sort them
	for (int i = 0; i < 4; i++)
	{
		sort(samples[i], NB_SAMPLES);
	}
	
	for (int j = 0; j < 4; j++)
	{
		durations[j] = (samples[j][NB_SAMPLES / 2] + samples[j][NB_SAMPLES / 2 -1] + samples[j][NB_SAMPLES / 2 +1])/3;//get median
	}

	//Test with aberrations removal
	/*//remove aberrations value of all samples by comparison with median value
	for (int j = 0; j < 4; j++)
	{
		durations[j] = samples[j][NB_SAMPLES/2];//get median
		nbAbberations[j] = 0;
		for (int i = 0; i < NB_SAMPLES; i++)
		{
			if (abs(1.0 - ((double)durations[j]) / ((double)samples[j][i])) > TOLERANCE_PERCENTAGE)
			{
				//1 abberation has been detected
				nbAbberations[j]++;
				//replace the value with the highest long value, it will be sorted and not used
				samples[j][i] = __LONG_MAX__;
			}
		}
	}

	//sort remaining samples
	for (int i = 0; i < 4; i++)
	{
		Serial.print(nbAbberations[i]);
		Serial.print(",");
		sort(samples[i], NB_SAMPLES);
	}
	Serial.println();
	
	//mean value of all remaining samples
	for (int j = 0; j < 4; j++)
	{
		durations[j] = 0;
		for (int i = 0; i < NB_SAMPLES-nbAbberations[j]; i++)
		{
			durations[j] += samples[j][i];
		}
		durations[j] /= (NB_SAMPLES-nbAbberations[j]);
	}*/

	Serial.print("All measurements made in ");
	Serial.print(millis() - beginMillis);
	Serial.println(" ms : ");
	
	for (int i = 0; i < 4; i++)
	{
		//deselect channel
		digitalWrite(selectA, 0);
		digitalWrite(selectB, 0);

		//process distances
		distance[i] = durations[i] * 340 / (2 * 10000);
		distanceD[i] = (double)durations[i] * 340.0 / (2.0 * 10000.0);
		distanceD[i] = sqrt(distanceD[i] * distanceD[i] - 1.2*1.2);

		Serial.print("\t");
		Serial.print(i);
		Serial.print(" : ");
		Serial.print(distanceD[i]);
		Serial.print(" cm ");
		Serial.print(durations[i]);
		Serial.println(" us");
	}
	
	double radius = 5.7;
	//45� angle application
	double l = radius / sqrt(2.0);
	Vector3D eX(1, 0, 0);
	Vector3D eY(0, 1, 0);
	Vector3D eZ(0, 0, 1);
	Vector3D ultrasonicPoints[4] = {
		Vector3D(l,l,0),
		Vector3D(l,-l,0),
		Vector3D(-l,-l,0),
		Vector3D(-l,l,0)
	};
	Vector3D rockPoints[4];
	//vector addition
	for (int i = 0; i < 4; i++)
	{
		rockPoints[i] = ultrasonicPoints[i] + Vector3D(0, 0, distanceD[i]);
	}
	double xAngles[4];
	double yAngles[4];
	//angle processing
	for (int i = 0; i < 4; i++)
	{
		Vector3D u = rockPoints[(i+1)%4] - rockPoints[i];
		Vector3D v = rockPoints[(i+2)%4] - rockPoints[i];
		/*
		Vector3D u = rockPoints[1] - rockPoints[0];
		Vector3D v = rockPoints[2] - rockPoints[0];
		*/
		Vector3D n = u.vectorProduct(v);
		Vector3D nXZ(n.x(), 0, n.z());
		Vector3D nYZ(0, n.y(), n.z());
		//x : pan, y : tilt
		xAngles[i] = nXZ.getAngleWith(eZ);
		yAngles[i] = nYZ.getAngleWith(eZ);

		if (nXZ.x() < 0)
			xAngles[i] = -xAngles[i];

		if (nYZ.y() < 0)
			yAngles[i] = -yAngles[i];
	}
	
	xAngle = 0;
	yAngle = 0;
	
	//mean angles values
	for (int i = 0; i < 4; i++)
	{
		xAngle += xAngles[i];
		yAngle += yAngles[i];
	}
	xAngle /= 4;
	yAngle /= 4;

	xPID.setGains(0.8, 0, 0.5);
	yPID.setGains(0.8, 0, 0.5);
	xPID.run(); //auto PID ! :)
	yPID.run(); //auto PID ! :)
	

	Serial.print("x angle : ");
	Serial.print(xAngle * 180.0 / PI);
	Serial.print(" / ");
	Serial.println(xlastAngle * 180.0 / PI);

	/*Serial.print("y angle : ");
	Serial.print(yAngle * 180.0 / PI);	
	Serial.print(" / ");
	Serial.println(ylastAngle * 180.0 / PI);*/
	

	//update only if needed
	if (abs(xAngle) > HYSTERESIS_ANGLE_RAD) {

		double xAngleServo = sin(xlastAngle) * 44 / 13;
		uint16_t value = getServoValue(xAngleServo);

		Serial.print("x servo angle : ");
		Serial.println(xAngleServo * 180 / PI);
		Serial.print("value : ");
		Serial.println(value);

		Wire.beginTransmission(0x40);
		Wire.write(CH0_END_L);
		uint8_t lowByte = (uint8_t)(value & 0x00FF);
		Wire.write(lowByte);
		Wire.endTransmission();

		Wire.beginTransmission(0x40);
		Wire.write(CH0_END_H);
		uint8_t highByte = (uint8_t)(value >> 8);
		Wire.write(highByte);
		Wire.endTransmission();
	}
	

	//update only if needed
	if (abs(yAngle) > HYSTERESIS_ANGLE_RAD) {

		double yAngleServo = -sin(ylastAngle) * 44 / 13;
		uint16_t value = getServoValue(yAngleServo);

		Serial.print("y servo angle : ");
		Serial.println(yAngleServo * 180 / PI);
		Serial.print("value : ");
		Serial.println(value);

		Wire.beginTransmission(0x40);
		Wire.write(CH1_END_L);
		uint8_t lowByte = (uint8_t)(value & 0x00FF);
		Wire.write(lowByte);
		Wire.endTransmission();

		Wire.beginTransmission(0x40);
		Wire.write(CH1_END_H);
		uint8_t highByte = (uint8_t)(value >> 8);
		Wire.write(highByte);
		Wire.endTransmission();
	}
}
