#pragma once
class Vector3D
{
private :
	double  _x, _y, _z;
public:
	Vector3D() : _x(0), _y(0), _z(0) {};
	Vector3D(double x, double y, double z) : _x(x), _y(y), _z(z) {};
	double x() { return _x; };
	double y() { return _y; };
	double z() { return _z; };
	double norm() {
		return sqrt(_x * _x + _y * _y + _z * _z);
	};
	Vector3D operator/(double a) {
		return Vector3D(_x / a, _y / a, _z / a);
	};
	Vector3D operator*(double a) {
		return Vector3D(_x * a, _y * a, _z * a);
	};
	Vector3D operator+(Vector3D v) {
		return Vector3D(_x + v._x, _y + v._y, _z + v._z);
	};
	Vector3D operator-(Vector3D v) {
		return Vector3D(_x - v._x, _y - v._y, _z - v._z);
	};
	double operator*(Vector3D v)
	{
		return _x*v._x + _y*v._y + _z*v._z;
	};
	Vector3D projectOn(Vector3D v) {
		Vector3D projection = v * (operator*(v) / (v.norm()*v.norm()));
	};
	double getAngleWith(Vector3D v) {
		double num = (*this) * v;
		return min(PI - acos(num / (norm() * v.norm())), acos(num / (norm() * v.norm())));
	};
	Vector3D vectorProduct(Vector3D v) {
		return Vector3D(_y*v._z - _z * v._y, _z*v._x - _x * v._z, _x*v._y - _y * v._x);
	};
};

